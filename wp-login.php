<!DOCTYPE html>
<html lang="tr">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Giriş &lsaquo; Respect Trade &#8212; WordPress</title>
	<meta name="robots" content="max-image-preview:large, noindex, noarchive">
        <script>
            /* <![CDATA[ */
            var rcewpp = {
                "ajax_url":"http://localhost/wp-admin/admin-ajax.php",
                "nonce": "e2353579db",
                "home_url": "http://localhost/",
                "settings_icon": 'http://localhost/wp-content/plugins/export-wp-page-to-static-html/admin/images/settings.png',
                "settings_hover_icon": 'http://localhost/wp-content/plugins/export-wp-page-to-static-html/admin/images/settings_hover.png'
            };
            /* ]]\> */
        </script>
        <link rel="stylesheet" id="dashicons-css" href="./wp-includes/css/dashicons.min.css?ver=6.1.1" media="all">
<link rel="stylesheet" id="buttons-css" href="./wp-includes/css/buttons.min.css?ver=6.1.1" media="all">
<link rel="stylesheet" id="forms-css" href="./wp-admin/css/forms.min.css?ver=6.1.1" media="all">
<link rel="stylesheet" id="l10n-css" href="./wp-admin/css/l10n.min.css?ver=6.1.1" media="all">
<link rel="stylesheet" id="login-css" href="./wp-admin/css/login.min.css?ver=6.1.1" media="all">
	<meta name="referrer" content="strict-origin-when-cross-origin">
		<meta name="viewport" content="width=device-width">
	<link rel="icon" href="./wp-content/uploads/2022/11/respecttrade_logo-150x150.png" sizes="32x32">
<link rel="icon" href="./wp-content/uploads/2022/11/respecttrade_logo-300x300.png" sizes="192x192">
<link rel="apple-touch-icon" href="./wp-content/uploads/2022/11/respecttrade_logo-300x300.png">
<meta name="msapplication-TileImage" content="./wp-content/uploads/2022/11/respecttrade_logo-300x300.png">
	</head>
	<body class="login no-js login-action-login wp-core-ui  locale-tr-tr">
	<script type="text/javascript">document.body.className = document.body.className.replace('no-js','js');</script>
		<div id="login">
		<h1><a href="https://wordpress.org/">WordPress'in desteğiyle</a></h1>
	
		<form name="loginform" id="loginform" action="http://localhost/wp-login.php" method="post">
			<p>
				<label for="user_login">Kullanıcı adı ya da e-posta adresi</label>
				<input type="text" name="log" id="user_login" class="input" value="" size="20" autocapitalize="off" autocomplete="username">
			</p>

			<div class="user-pass-wrap">
				<label for="user_pass">Parola</label>
				<div class="wp-pwd">
					<input type="password" name="pwd" id="user_pass" class="input password-input" value="" size="20" autocomplete="current-password">
					<button type="button" class="button button-secondary wp-hide-pw hide-if-no-js" data-toggle="0" aria-label="Parolayı göster">
						<span class="dashicons dashicons-visibility" aria-hidden="true"></span>
					</button>
				</div>
			</div>
						<p class="forgetmenot"><input name="rememberme" type="checkbox" id="rememberme" value="forever"> <label for="rememberme">Beni hatırla</label></p>
			<p class="submit">
				<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Giriş">
									<input type="hidden" name="redirect_to" value="http://localhost/wp-admin/">
									<input type="hidden" name="testcookie" value="1">
			</p>
		</form>

					<p id="nav">
				<a href="./?action=lostpassword">Parolanızı mı unuttunuz?</a>			</p>
					<script type="text/javascript">function wp_attempt_focus() {setTimeout( function() {try {d = document.getElementById( "user_login" );d.focus(); d.select();} catch( er ) {}}, 200);}
wp_attempt_focus();
if ( typeof wpOnload === 'function' ) { wpOnload() }</script>
				<p id="backtoblog">
			<a href="./index.html">&larr; Respect Trade sitesine geri dön</a>		</p>
			</div>
				<div class="language-switcher">
				<form id="language-switcher" action="" method="get">

					<label for="language-switcher-locales">
						<span class="dashicons dashicons-translation" aria-hidden="true"></span>
						<span class="screen-reader-text">Dil</span>
					</label>

					<select name="wp_lang" id="language-switcher-locales"><option value="en_US" lang="en" data-installed="1">English (United States)</option>
<option value="tr_TR" lang="tr" selected data-installed="1">Türkçe</option></select>
					
					
					
						<input type="submit" class="button" value="Değiştir">

					</form>
				</div>
				<script src="./wp-includes/js/jquery/jquery.min.js?ver=3.6.1" id="jquery-core-js"></script>
<script src="./wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" id="jquery-migrate-js"></script>
<script id="zxcvbn-async-js-extra">var _zxcvbnSettings = {"src":".\/wp-includes\/js\/zxcvbn.min.js"};</script>
<script src="./wp-includes/js/zxcvbn-async.min.js?ver=1.0" id="zxcvbn-async-js"></script>
<script src="./wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.13.9" id="regenerator-runtime-js"></script>
<script src="./wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0" id="wp-polyfill-js"></script>
<script src="./wp-includes/js/dist/hooks.min.js?ver=4169d3cf8e8d95a3d6d5" id="wp-hooks-js"></script>
<script src="./wp-includes/js/dist/i18n.min.js?ver=9e794f35a71bb98672ae" id="wp-i18n-js"></script>
<script id="wp-i18n-js-after">
wp.i18n.setLocaleData( { 'text directionltr': [ 'ltr' ] } );
</script>
<script id="password-strength-meter-js-extra">var pwsL10n = {"unknown":"Parola gücü bilinmiyor","short":"Çok zayıf","bad":"Zayıf","good":"Orta","strong":"Güçlü","mismatch":"Parolalar uyuşmuyor"};</script>
<script id="password-strength-meter-js-translations">( function( domain, translations ) {
	var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
	localeData[""].domain = domain;
	wp.i18n.setLocaleData( localeData, domain );
} )( "default", {"translation-revision-date":"2022-10-31 18:09:02+0000","generator":"GlotPress\/4.0.0-alpha.3","domain":"messages","locale_data":{"messages":{"":{"domain":"messages","plural-forms":"nplurals=2; plural=n > 1;","lang":"tr"},"%1$s is deprecated since version %2$s! Use %3$s instead. Please consider writing more inclusive code.":["%2$s sürümünden bu yana %1$s devre dışı bırakılmıştır! Bunun yerine %3$s kullanın. Lütfen daha kapsamlı kod yazmayı göz önünde bulundurun."]}},"comment":{"reference":"wp-admin\/js\/password-strength-meter.js"}} );</script>
<script src="./wp-admin/js/password-strength-meter.min.js?ver=6.1.1" id="password-strength-meter-js"></script>
<script src="./wp-includes/js/underscore.min.js?ver=1.13.4" id="underscore-js"></script>
<script id="wp-util-js-extra">var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};</script>
<script src="./wp-includes/js/wp-util.min.js?ver=6.1.1" id="wp-util-js"></script>
<script id="user-profile-js-extra">var userProfileL10n = {"user_id":"0","nonce":"156e093144"};</script>
<script id="user-profile-js-translations">( function( domain, translations ) {
	var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
	localeData[""].domain = domain;
	wp.i18n.setLocaleData( localeData, domain );
} )( "default", {"translation-revision-date":"2022-10-31 18:09:02+0000","generator":"GlotPress\/4.0.0-alpha.3","domain":"messages","locale_data":{"messages":{"":{"domain":"messages","plural-forms":"nplurals=2; plural=n > 1;","lang":"tr"},"Your new password has not been saved.":["Yeni parolanız kaydedilemedi."],"Hide":["Gizle"],"Show":["Göster"],"Confirm use of weak password":["Zayıf parola kullanmayı onayla"],"Hide password":["Parolayı gizle"],"Show password":["Parolayı göster"]}},"comment":{"reference":"wp-admin\/js\/user-profile.js"}} );</script>
<script src="./wp-admin/js/user-profile.min.js?ver=6.1.1" id="user-profile-js"></script>
	<div class="clear"></div>
	</body>
	</html>